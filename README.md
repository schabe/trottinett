trottinett
==========

A simple network interface manager for GNU/Linux.

Trottinett was written to allow a laptop computer to quickly switch between a wired and a wifi connection, optionally using the same MAC address for both interfaces. Its mode of operation is very simple:

### If a network cable is connected

* Disconnect the wifi interface
* Run DHCP on the wired interface

### If no network cable is connected

* Use [wpa_supplicant](https://w1.fi/wpa_supplicant/) to connect to a wireless network
* Run DHCP on the wireless interface

## Dependencies

In order to run trottinett you need some additional software on your machine:

* A DHCP client. Currently only [dhcpcd](https://roy.marples.name/projects/dhcpcd/index) is supported. Make sure to disable persistent interface configuration in dhcpcd.conf.
* [wpa_supplicant](https://w1.fi/wpa_supplicant/) to manage your wifi settings. You may want to use `wpa_gui` for extra comfort.

## Packages

An ebuild for Gentoo Linux is available on [git.ott.net](https://git.ott.net/portage-overlay/tree/net-misc/trottinett).
