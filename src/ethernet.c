#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <sys/ioctl.h>

static void
print_mac(const unsigned char *mac, char *out, size_t len)
{
	unsigned int i;
	char *tmp;

	if (len < 18)
	{
		syslog(LOG_ERR, "Buffer too short, not doing anything");
		return;
	}

	tmp = out;
	for (i = 0; i < ETH_ALEN; ++i)
	{
		sprintf(tmp, "%02x:", mac[i]);
		tmp += 3;
	}
	out[17] = 0;
}

int
get_mac(const char *ifname, unsigned char *out)
{
	struct ifreq ifr;
	int sock;
	char buf[32];

	memset(&ifr, 0, sizeof(struct ifreq));
	memset(buf, 0, sizeof(buf));

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sock == -1)
	{
		syslog(LOG_ERR, "socket: %s", strerror(errno));
		return 1;
	}

	snprintf(ifr.ifr_name, IFNAMSIZ, "%s", ifname);

	if (ioctl(sock, SIOCGIFHWADDR, &ifr) != 0)
	{
		syslog(LOG_ERR, "%s: %s", ifname, strerror(errno));
		close(sock);
		return 1;
	}

	memcpy(out, ifr.ifr_hwaddr.sa_data, ETH_ALEN);
	close(sock);

	print_mac(out, buf, sizeof(buf));
	syslog(LOG_DEBUG, "[ ] MAC for %s: %s", ifname, buf);

	return 0;
}

int
set_mac(const char *ifname, const unsigned char *mac)
{
	struct ifreq ifr;
	int sock;
	unsigned char buf[32];

	memset(&ifr, 0, sizeof(struct ifreq));
	memset(buf, 0, sizeof(buf));

	/* Don't set the address if it is already being used */
	get_mac(ifname, buf);
	if (!strncmp((char *)buf, (char *)mac, ETH_ALEN))
	{
		return 0;
	}

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sock == -1)
	{
		syslog(LOG_ERR, "socket: %s", strerror(errno));
		return 1;
	}

	snprintf(ifr.ifr_name, IFNAMSIZ, "%s", ifname);
	ifr.ifr_hwaddr.sa_family = ARPHRD_ETHER;
	memcpy(ifr.ifr_hwaddr.sa_data, mac, ETH_ALEN);

	if (ioctl(sock, SIOCSIFHWADDR, &ifr) != 0)
	{
		syslog(LOG_ERR, "%s: %s", ifname, strerror(errno));
		close(sock);
		return 1;
	}

	close(sock);
	return 0;
}

int
iface_running(const char *ifname)
{
	struct ifreq ifr;
	short flags;
	int fd;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		syslog(LOG_ERR, "socket: %s", strerror(errno));
		return 0;
	}

	memset(&ifr, 0, sizeof(struct ifreq));
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", ifname);

	if (ioctl(fd, SIOCGIFFLAGS, &ifr) != 0)
	{
		syslog(LOG_ERR, "%s: %s", ifname, strerror(errno));
		close(fd);
		return 0;
	}
	close(fd);

	flags = ifr.ifr_flags;
	return (flags & IFF_RUNNING) != 0;
}

int
raise_iface(const char *ifname)
{
	struct ifreq ifr;
	int fd;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		syslog(LOG_ERR, "socket: %s", strerror(errno));
		return 1;
	}

	memset(&ifr, 0, sizeof(struct ifreq));
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", ifname);

	/* get current flags */
	if (ioctl(fd, SIOCGIFFLAGS, &ifr) != 0)
	{
		syslog(LOG_ERR, "%s: %s", ifname, strerror(errno));
		close(fd);
		return 1;
	}

	/* set new flags */
	ifr.ifr_flags |= IFF_UP;
	if (ioctl(fd, SIOCSIFFLAGS, &ifr) != 0)
	{
		syslog(LOG_ERR, "%s: %s", ifname, strerror(errno));
		close(fd);
		return 1;
	}
	close(fd);

	return 0;
}
