#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <linux/limits.h>
#include <sys/stat.h>
#include <sys/types.h>

static void
run_dhcpcd(const char *ifname)
{
	pid_t pid;

	pid = fork();

	if (pid == -1)
	{
		syslog(LOG_ERR, "fork: %s", strerror(errno));
		return;
	}
	else if (pid > 0)
	{
		/* we are the parent */
		return;
	}

	if (execlp("dhcpcd", "dhcpcd", "--noipv4ll", ifname, NULL) == -1)
	{
		syslog(LOG_ERR, "dhcpcd: %s", strerror(errno));
	}

	_exit(EXIT_FAILURE);
}

static int
get_pid(const char *fn)
{
	int fd, pid;
	char buf[16];

	fd = open(fn, 0);
	if (fd == -1)
	{
		syslog(LOG_ERR, "%s: %s", fn, strerror(errno));
		syslog(LOG_ERR, "[!] Failed to open DHCP client PID file");
		return 0;
	}

	memset(buf, 0, sizeof(buf));
	if (read(fd, buf, sizeof(buf) - 1) == -1)
	{
		syslog(LOG_ERR, "%s: %s", fn, strerror(errno));
		syslog(LOG_ERR, "[!] Failed to read DHCP client PID file");
		close(fd);
		return 0;
	}
	close(fd);

	pid = atoi(buf);
	if (pid == 0)
	{
		syslog(LOG_ERR, "[!] Failed to parse PID: %s", buf);
	}

	return pid;
}

static int
pid_running(int pid)
{
	char path[PATH_MAX];

	snprintf(path, sizeof(path), "/proc/%d", pid);
	return access(path, R_OK) == 0;
}

static void
get_dhcpcd_pid_file_name(char *buf, size_t len, const char *ifname)
{
	struct stat st;

	memset(&st, 0, sizeof(struct stat));

	if ((stat("/run/dhcpcd", &st) == 0) && (st.st_mode & S_IFDIR))
	{
		snprintf(buf, len, "/run/dhcpcd/%s.pid", ifname);
	}
	else
	{
		snprintf(buf, len, "/run/dhcpcd-%s.pid", ifname);
	}
}

void
run_dhcp(const char *ifname)
{
	char path[PATH_MAX];
	int pid;

	get_dhcpcd_pid_file_name(path, sizeof(path), ifname);
	syslog(LOG_DEBUG, "[ ] Checking PID in %s", path);

	if (access(path, R_OK) != 0)
	{
		syslog(LOG_INFO, "%s: %s", path, strerror(errno));
		syslog(LOG_INFO, "[-] DHCP client not running on %s", ifname);
		run_dhcpcd(ifname);
		return;
	}

	pid = get_pid(path);
	if (pid < 1)
	{
		syslog(LOG_DEBUG, "[!] Aborting, no valid PID found");
		return;
	}

	if (pid_running(pid))
	{
		syslog(LOG_INFO, "[+] DHCP client running on %s", ifname);
	}
	else
	{
		syslog(LOG_ERR, "%s: %s", path, strerror(errno));
		syslog(LOG_ERR, "[!] DHCP client no longer running");
		run_dhcpcd(ifname);
	}
}

void
stop_dhcp(const char *ifname)
{
	char path[PATH_MAX];
	int pid;

	get_dhcpcd_pid_file_name(path, sizeof(path), ifname);
	syslog(LOG_DEBUG, "[ ] Checking PID in %s", path);

	if (access(path, R_OK) != 0)
	{
		syslog(LOG_DEBUG, "[+] DHCP client not running on %s", ifname);
		return;
	}

	pid = get_pid(path);
	if (pid < 1)
	{
		syslog(LOG_DEBUG, "[!] Aborting, no valid PID found");
		return;
	}

	if (pid_running(pid))
	{
		syslog(LOG_INFO, "[-] DHCP client running on %s, terminating",
									ifname);
		kill(pid, SIGTERM);
	}
	else
	{
		syslog(LOG_ERR, "%s: %s", path, strerror(errno));
		syslog(LOG_ERR, "[!] DHCP client no longer running");
	}
}
