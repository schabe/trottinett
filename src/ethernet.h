#ifndef TROT_ETHERNET_H
#define TROT_ETHERNET_H

int get_mac(const char *ifname, unsigned char *mac);
int set_mac(const char *ifname, const unsigned char *mac);
int iface_running(const char *ifname);
int raise_iface(const char *ifname);

#endif
