#ifndef TROT_DHCP_H
#define TROT_DHCP_H

void run_dhcp(const char *ifname);
void stop_dhcp(const char *ifname);

#endif
