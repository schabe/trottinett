#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

static void
run_wpa_supplicant(const char *ifn)
{
	pid_t pid;
	char buf[16];

	pid = fork();

	if (pid == -1)
	{
		syslog(LOG_ERR, "fork: %s", strerror(errno));
		return;
	}
	else if (pid > 0)
	{
		/* we are the parent */
		return;
	}

	snprintf(buf, sizeof(buf), "-i%s", ifn);
	if (execlp("wpa_supplicant", "wpa_supplicant", "-Dnl80211", buf,
				"-c/etc/wpa_supplicant.conf", NULL) == -1)
	{
		syslog(LOG_ERR, "dhcpcd: %s", strerror(errno));
	}

	_exit(EXIT_FAILURE);
}

static void
wpa_cmd(const char *path, const char *cmd)
{
	int fd;
	struct sockaddr_un sa;
	size_t len;

	memset(&sa, 0, sizeof(struct sockaddr_un));

	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		syslog(LOG_ERR, "socket: %s", strerror(errno));
		return;
	}

	sa.sun_family = AF_UNIX;
	snprintf(sa.sun_path, sizeof(sa.sun_path), "%s", path);

	len = sizeof(struct sockaddr_un);
	if (connect(fd, (const struct sockaddr *) &sa, len) == -1)
	{
		syslog(LOG_ERR, "%s: %s", path, strerror(errno));
		close(fd);
		return;
	}

	if ((size_t) send(fd, cmd, strlen(cmd), 0) != strlen(cmd))
	{
		syslog(LOG_ERR, "%s: %s", cmd, strerror(errno));
	}

	close(fd);
}

void
run_wpa(const char *ifn)
{
	char path[128];
	struct stat st;

	memset(&st, 0, sizeof(struct stat));
	snprintf(path, sizeof(path), "/run/wpa_supplicant/%s", ifn);

	if (stat(path, &st) != 0)
	{
		syslog(LOG_INFO, "%s: %s", path, strerror(errno));
		syslog(LOG_INFO, "[-] wpa_supplicant not running on %s", ifn);
		run_wpa_supplicant(ifn);
		return;
	}

	syslog(LOG_INFO, "[+] wpa_supplicant running on %s", ifn);
}

void
stop_wpa(const char *ifn)
{
	char path[128];
	struct stat st;

	memset(&st, 0, sizeof(struct stat));
	snprintf(path, sizeof(path), "/run/wpa_supplicant/%s", ifn);

	if (stat(path, &st) != 0)
	{
		syslog(LOG_DEBUG, "[+] wpa_supplicant not running on %s", ifn);
		return;
	}

	syslog(LOG_INFO, "[-] wpa_supplicant running on %s, stopping", ifn);

	wpa_cmd(path, "DISCONNECT");
	sleep(1);
	wpa_cmd(path, "TERMINATE");
}
