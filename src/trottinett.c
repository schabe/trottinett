/*

trottinett -- Simple Network Interface Organizer
Copyright (C) 2016-2017  Stefan Ott

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>

#include <sys/sysinfo.h>
#include <sys/un.h>

#include <linux/if_ether.h>

#include "config.h"
#include "dhcp.h"
#include "ethernet.h"
#include "wpa.h"

#define IFACE_WIRED "eth0"
#define IFACE_WIFI "wlan0"

static int running;

struct ifspec
{
	char *name;
	unsigned char mac[18];
	struct ifspec *next;
};

static void
terminate(int arg)
{
	fprintf(stderr, "Received signal %d, terminating...\n", arg);
	running = 0;
}

static void
help()
{
	printf("This is %s\n", PACKAGE_STRING);
	printf("\n");
	printf("Usage: trottinett [options]\n\n");
	printf("Options:\n\n");
	printf("  -d            Enable debug mode\n");
	printf("  -h            Show this help\n");
	printf("  -m IFACE	Use IFACE's MAC address\n");
	printf("\n");
	printf("Report bugs at %s\n", PACKAGE_URL);
}

static void
update(struct ifspec *if_wired, struct ifspec *if_wifi, struct ifspec *if_mac)
{
	if (iface_running(if_wired->name))
	{
		syslog(LOG_DEBUG, "[+] %s up", if_wired->name);
		if (if_mac->name)
		{
			set_mac(if_wired->name, if_mac->mac);
			set_mac(if_wifi->name, if_wifi->mac);
		}
		stop_dhcp(if_wifi->name);
		stop_wpa(if_wifi->name);
		run_dhcp(if_wired->name);
	}
	else
	{
		syslog(LOG_DEBUG, "[-] %s down, using wifi", if_wired->name);
		if (if_mac->name)
		{
			set_mac(if_wired->name, if_wired->mac);
			set_mac(if_wifi->name, if_mac->mac);
		}
		stop_dhcp(if_wired->name);
		run_wpa(if_wifi->name);
		run_dhcp(if_wifi->name);
	}
}

static int
run(struct ifspec *if_wired, struct ifspec *if_wifi, struct ifspec *if_mac)
{
	struct sysinfo info;
	long then, now;

	then = 0;

	while (running)
	{
		memset(&info, 0, sizeof(struct sysinfo));
		if (sysinfo(&info) != 0)
		{
			syslog(LOG_ERR, "sysinfo: %s", strerror(errno));
			memset(&info, 0, sizeof(struct sysinfo));
		}
		now = info.uptime;

		syslog(LOG_DEBUG, "Update %ld", now);

		if ((now - then > 5) && (then > 0))
		{
			syslog(LOG_INFO, "A long time has passed, restarting");
			stop_dhcp(if_wifi->name);
			stop_dhcp(if_wired->name);
			stop_wpa(if_wifi->name);
		}

		update(if_wired, if_wifi, if_mac);

		then = now;
		sleep(1);
	}

	/* cleanup */
	stop_dhcp(if_wired->name);
	stop_dhcp(if_wifi->name);
	stop_wpa(if_wifi->name);

	/* restore original MAC addresses */
	if (if_mac->name)
	{
		set_mac(if_wired->name, if_wired->mac);
		set_mac(if_wifi->name, if_wifi->mac);
	}

	return EXIT_SUCCESS;
}

static int
load_interface(struct ifspec *iface)
{
	if (get_mac(iface->name, iface->mac) != 0)
	{
		fprintf(stderr, "%s: failed to get MAC address\n", iface->name);
		return 1;
	}

	return 0;
}

static int
load_interfaces(struct ifspec *iface)
{
	for (; iface; iface = iface->next)
	{
		if (load_interface(iface) != 0)
		{
			return 1;
		}
	}

	return 0;
}

int
main(int argc, char **argv)
{
	char c;
	int logp, logopt;
	struct ifspec if_wired, if_wifi, if_mac;

	/* initialize values */
	memset(&if_wired, 0, sizeof(struct ifspec));
	memset(&if_wifi, 0, sizeof(struct ifspec));
	memset(&if_mac, 0, sizeof(struct ifspec));

	if_wired.next = &if_wifi;
	if_wifi.next = &if_mac;

	if_wired.name = IFACE_WIRED;
	if_wifi.name = IFACE_WIFI;

	running = 1;
	logp = LOG_NOTICE;
	logopt = LOG_PERROR;

	/* parse options */
	while ((c = getopt(argc, argv, "dhm:")) != -1)
	{
		switch (c)
		{
		case 'd':
			logp = LOG_DEBUG;
			break;
		case 'h':
			help();
			return 0;
		case 'm':
			if_mac.name = optarg;
			break;
		default:
			help();
			return EXIT_FAILURE;
		}
	}

	/* clean exit on SIGINT */
	signal(SIGINT, terminate);
	/* ignore return values from child processes */
	signal(SIGCHLD, SIG_IGN);

	/* configure logging */
	openlog(NULL, logopt, LOG_DAEMON);
	setlogmask(LOG_UPTO(logp));

	/* load MAC addresses if we need them */
	if ((if_mac.name) && (load_interfaces(&if_wired) != 0))
	{
		return EXIT_FAILURE;
	}

	/* raise wired interface on startup */
	if (raise_iface(if_wired.name) != 0)
	{
		fprintf(stderr, "Failed to raise %s\n", if_wired.name);
	}

	/* run the application */
	return run(&if_wired, &if_wifi, &if_mac);
}
